const express = require('express')
const app = express()

let todos =[ //ทำรูปเเบบตารางarray
{
    name: 'Boom',
    id: 1
},
{   name: 'kantart',
    id: 2
}
]

//เหมือนSELECT * FROM TODO
app.get('/todos',(req,res) => {
    res.send(todos) //ข้อมูลที่จะส่งกลับtodos
} )

//INSERT INTO TODO
app.post('/todos',(req,res) => {
    let newTodo = {
        name: 'Read a book',
        id: 3
    }

    todos.push(newTodo) //push ของ array
    res.status(201).send()
} )

app.listen(3000,() =>{
    console.log('TODO API Started at port 3000')
})